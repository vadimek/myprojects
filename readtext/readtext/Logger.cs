﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace readtext
{
    class Logger
    {
        private int numbeOfErrors = 0;

        public void IncreaseNumberOfErrors()
        {
            numbeOfErrors = ++numbeOfErrors;
        }

        public void WriteNumberOfErrors()
        {
            string additionalMessage = "";
            switch (numbeOfErrors)
            {
                case 0: additionalMessage = "Super! Seš pán";
                    break;
                case 1: additionalMessage = "Not bad";
                    break;
                default: additionalMessage = "Mohlo to být lepší. Zkus znovu.";
                    break;
            }
                
            Console.WriteLine("\nYou have made " + numbeOfErrors + " type errors. " + additionalMessage);
        }
    }
}
