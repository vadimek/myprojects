﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace readtext
{
    class Program
    {
        static void Main(string[] args)

        {
            FileStream stream = new FileStream("New Text Document.txt", FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string textFromFile = sr.ReadToEnd();
            stream.Close();
            Console.WriteLine("Type this text: \n \n" + textFromFile + "\n");
            char[] charsInText = textFromFile.ToCharArray();

            Logger mylogger = new Logger();
               
            for (int a = 0; a<charsInText.Length;)
            {
                ConsoleKeyInfo charToCompare = Console.ReadKey(intercept: true);

                if (charToCompare.KeyChar == charsInText[a])
                {
                    
                    Console.Write(charToCompare.KeyChar);
                   a++;
                }
                else
                {
                    mylogger.IncreaseNumberOfErrors();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(charToCompare.KeyChar);

                    while (Console.ReadKey().Key != ConsoleKey.Backspace)
                    {                        
                    }

                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(" \b");
                }
            }

            mylogger.WriteNumberOfErrors();
            Console.ReadKey();
        }
    }
}
    

